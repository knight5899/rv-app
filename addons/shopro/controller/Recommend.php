<?php

namespace addons\shopro\controller;
use  think\Db;

class Recommend extends Base
{

    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];
    protected $relationSearch = true;

    public function _initialize()
    {
        parent::_initialize(); // TODO: Change the autogenerated stub
        $this->model = new \addons\shopro\model\Recommend;
    }

    /**
     * 推荐官申请
     */
    public function apply()
    {

        $params = $this->request->post();
        if(!$params['title']){
            $this->error('渠道不能为空', null, -1);
        }
        if(!$params['username']){
            $this->error('姓名不能为空', null, -1);
        }
        if(!$params['mobile']){
            $this->error('手机不能为空', null, -1);
        }

        $card = $this->model;
        $card->type = $params['type'];
        $card->user_id = $this->auth->id;
        $card->title = $params['title'];
        $card->username = $params['username'];
        $card->mobile = $params['mobile'];
        $card->message = $params['message'];
        $card->save();
        $this->success('提交成功,稍后我们会联系您');
    }

    /**
     * 我的下级用户
     */
    public function myUser(){
        $user = new \addons\shopro\model\User;
        $list = $user
        ->where(['pid'=>$this->auth->id])
        ->field('id,nickname,avatar')
        ->paginate();
        $this->success('我的下级用户',$list);
    }

    /**
     * 我的用户下单
     */
    public function myUserOrder(){
        $user = new \addons\shopro\model\Order;
        $list = $user
            ->where('user')
            ->where(['user_id'=>$this->auth->id])
            ->field('id,nickname,avatar')
            ->paginate();
        $this->success('我的下级用户',$list);
    }



    /**
     * 渠道分销合作
     */
    public function store()
    {
        $params = $this->request->get();
        $data = \addons\shopro\model\Goods::getGoodsStore($params);

        $this->success('自提列表', $data);
    }


    // 秒杀列表
    public function seckillList() {
        $params = $this->request->get();

        $this->success('秒杀商品列表', \addons\shopro\model\Goods::getSeckillGoodsList($params));
    }


    // 拼团列表
    public function grouponList() {
        $params = $this->request->get();

        $this->success('拼团商品列表', \addons\shopro\model\Goods::getGrouponGoodsList($params));
    }


    public function activity()
    {
        $activity_id = $this->request->get('activity_id');
        $activity = \addons\shopro\model\Activity::get($activity_id);
        if (!$activity) {
            $this->error('活动不存在', null, -1);
        }

        $goods = \addons\shopro\model\Goods::getGoodsList(['goods_ids' => $activity->goods_ids]);
        $activity->goods = $goods;

        $this->success('活动列表', $activity);
    }

    public function favorite()
    {
        $params = $this->request->post();
        $result = \addons\shopro\model\UserFavorite::edit($params);
        $this->success($result ? '收藏成功' : '取消收藏', $result);
    }

    public function favoriteList()
    {
        $data = \addons\shopro\model\UserFavorite::getGoodsList();
        $this->success('商品收藏列表', $data);
    }


    public function viewDelete()
    {
        $params = $this->request->post();
        $result = \addons\shopro\model\UserView::del($params);
        $this->success('删除成功', $result);
    }


    public function viewList()
    {
        $data = \addons\shopro\model\UserView::getGoodsList();
        $this->success('商品浏览列表', $data);
    }



}
