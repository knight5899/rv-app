<?php

namespace addons\shopro\controller;


use addons\shopro\model\Config;

class Schedule extends Base
{

    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];
    protected $relationSearch = true;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \addons\shopro\model\Schedule;
    }

    /**
     * 房车日期预订查询
     */
    public function index()
    {
        $card = $this->model;
        $cardList = $card
            ->group('date')
            ->order('date asc,num desc')
            ->field('date,sum(num) as num')
            ->select();
        $config = Config::where('name', 'shopro')->find();
        $config = json_decode($config['value'], true);
        foreach ($cardList as $k=>&$v){
                $v['isfull'] = false;
            if($v['num']>=$config['fanche_num']){
                $v['isfull'] = true;
            }
        }
        $this->success('房车订满查询', $cardList);
    }
}
