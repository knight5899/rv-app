<?php

namespace addons\shopro\model;
use think\Log;
use think\Model;

/**
 * 区域数据
 */
class Schedule extends Model
{

    // 表名,不含前缀
    protected $name = 'shopro_schedule';
    protected $hidden = ['createtime'];
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';


    // 追加属性
    protected $append = [
    ];

    /**
     * @param $data  预定日期
     * @param $type  预定或取消  1=预定,2=取消
     */
    public static function addOrdel($data,$user_id,$type){
        Log::write("schedule-addOrdel");
        if($type==1){
            $day_num = 1;
            $one_time  = $data['start_time'];
            if($data['start_time']+1*60*60*24!=$data['end_time']){//订二天
                $day_num = 2;
                $two_time = $data['start_time']+1*60*60*24;
            }
            //获取积分配置
            $config = Config::where('name', 'shopro')->find();
            $config = json_decode($config['value'], true);
            $fanche_num = $config['fanche_num'];
            $count = self::where(['date'=>$data['start_time']])->count();//查询已有数据
            if($fanche_num<=$count){
                return false;
            }
            $data = ['date'=>$one_time,'user_id'=>$user_id,'order_id'=>$data['order_id'],'createtime'=>time()];
            self::insert($data);
            if($day_num==2){

                $data = ['date'=>$two_time,'user_id'=>$user_id,'order_id'=>$data['order_id'],'createtime'=>time()];
                self::insert($data);
            }
        }else{

            self::where('order_id',$data->order_id)->delete();
        }
    }



}
