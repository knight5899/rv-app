<?php

namespace addons\shopro\model;

use think\Model;
use addons\shopro\exception\Exception;
use think\Db;

/**
 * 订单操作日志
 */
class OrderAction extends Model
{

    // 表名,不含前缀
    protected $name = 'shopro_order_action';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    protected $hidden = ['createtime', 'updatetime', 'deletetime'];
    // //列表动态隐藏字段
    // protected static $list_hidden = ['content', 'params', 'images', 'service_ids'];

    public static function operAdd($order = null, $item = null, $user = null, $type = 'user', $remark = '')
    {
        $oper_id = empty($user) ? 0 : (is_array($user) ? $user['id'] : $user->id);
        $self = new self();
        $self->order_id = $order['id'];
        $self->order_item_id = is_null($item) ? 0 : $item['id'];
        $self->oper_type = $type;
        $self->oper_id = $oper_id;
        $self->order_status = is_null($order) ? 0 : $order['status'];
        $self->dispatch_status = is_null($item) ? 0 : $item['dispatch_status'];
        $self->comment_status = is_null($item) ? 0 : $item['comment_status'];
        $self->aftersale_status = is_null($item) ? 0 : $item['aftersale_status'];
        $self->refund_status = is_null($item) ? 0 : $item['refund_status'];
        $self->remark = $remark;
        $self->save();

        if(!isset($item['channel_type'])){
            return $self;
        }
        //处理违约记录
        $Unpromise = new Unpromise();
        $level = 0;
        //非会员不需要交违约金
        $card = new Card();
        $card = $card->where(['user_id'=>$order['user_id'],'pay_status'=>1,'expire_time'=>['gt',time()]])->find();
        if(in_array($item['aftersale_status'],[-1,0,2])){//0=用户主动取消退款，-1=后台不同意退款,2=后台同意退款或售后完成

            if(in_array($item['aftersale_status'],[2])){//后台同意退款和售后完成应该变更订单状态为
                Db::name('shopro_order')->where('id',$order['id'])->update(['status'=>2]);
                return $self;
            }
            $Unpromise->where(['order_id'=>$order['id'],'channel_type'=>'car'])->delete();//0=用户主动取消退款，-1=后台不同意退款需要删除处罚记录
        }elseif($item['aftersale_status']==1 && $card){//取消订单
            if(!empty($item['start_time'])){
                if($item['start_time']<time()){//当天取消订单的
                    $level = 2;
                    $mark = "取车当天取消订单罚500元违约金，并记录一次警告";
                }elseif($item['start_time']-60*60*48<time()){
                    $level = 1;
                    $mark = "48小时内取消订单罚款500元违约金";
                }
            }
            if($level==2){
                $result = $Unpromise->where(['user_id'=>$oper_id,'level'=>2])->find();
                if(!empty($result)){
                    $level = 3;
                    $mark = "2次在平台当天取消订单，将罚款500元违约金，并限制下单45天";
                    $Unpromise->open_time = time()+60*60*24*45;//解封时间
                }
            }
            if($level!=0){
                $Unpromise->user_id = $oper_id;
                $Unpromise->order_id = $order['id'];
                $Unpromise->channel_type = 'car';
                $Unpromise->pay_price = 500;
                $Unpromise->level = $level;
                $Unpromise->mark = $mark;
                $Unpromise->save();
            }
        }


        return $self;
    }

}
