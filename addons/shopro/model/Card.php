<?php

namespace addons\shopro\model;
use think\Model;

/**
 * 区域数据
 */
class Card extends Model
{

    // 表名,不含前缀
    protected $name = 'shopro_card';
    protected $hidden = ['createtime', 'updatetime', 'deletetime','active_code','open_switch'];
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
    ];


    public function cardType()
    {
        return $this->belongsTo('addons\shopro\model\CardType', '', '', [], 'LEFT')->setEagerlyType(0);
    }



}
