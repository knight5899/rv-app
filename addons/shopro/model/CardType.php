<?php

namespace addons\shopro\model;

use think\Model;

/**
 * 区域数据
 */
class CardType extends Model
{

    // 表名,不含前缀
    protected $name = 'shopro_card_type';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';
    protected $hidden = ['createtime', 'updatetime', 'deletetime','status_switch'];

    // 追加属性
    protected $append = [
    ];

}
