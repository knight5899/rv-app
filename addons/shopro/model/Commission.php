<?php

namespace addons\shopro\model;
use think\Model;

/**
 * 区域数据
 */
class Commission extends Model
{

    // 表名,不含前缀
    protected $name = 'shopro_commission';
    protected $hidden = ['createtime', 'deletetime'];

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
    ];

    public function user()
    {
        return $this->belongsTo('addons\shopro\model\User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
