<?php

namespace addons\shopro\model;
use think\Model;

/**
 * 区域数据
 */
class Unpromise extends Model
{

    // 表名,不含前缀
    protected $name = 'shopro_unpromise';
    protected $hidden = ['updatetime'];
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';


    // 追加属性
    protected $append = [
    ];


}
