<?php

namespace app\admin\model\shopro\recommend;

use app\admin\model\User;
use think\Model;
use traits\model\SoftDelete;

class Recommend extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'shopro_recommend';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'type_text',
        'status_switch_text'
    ];
    

    
    public function getTypeList()
    {
        return ['1' => __('Type 1'), '2' => __('Type 2'),'3' => __('Type 3')];
    }

    public function getStatusSwitchList()
    {
        return ['1' => __('Status_switch 1'), '0' => __('Status_switch 0')];
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getStatusSwitchTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status_switch']) ? $data['status_switch'] : '');
        $list = $this->getStatusSwitchList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function setStatusSwitchAttr($value,$data){
        if($value==1){
            if($data['type']==1){//推荐的设置
                $user  = new User();
                $user->where(['id'=>$data['user_id']])->update(['recommend'=>1]);
            }
        }
        return $value;
    }




}
