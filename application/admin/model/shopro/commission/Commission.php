<?php

namespace app\admin\model\shopro\commission;

use addons\shopro\model\UserWalletLog;
use app\admin\model\shopro\user\User;
use think\Db;
use think\Model;


class Commission extends Model
{

    

    

    // 表名
    protected $name = 'shopro_commission';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'status_text'
    ];
    

    
    public function getStatusList()
    {
        return ['0' => __('Status 0'), '1' => __('Status 1'), '2' => __('Status 2')];
    }


    public function getStatusTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['status']) ? $data['status'] : '');
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    /**
     * 修改状态
     * @param $value
     * @param $data
     * @return mixed|stringw
     */
    public function setStatusAttr($value, $data)
    {
        if($value==1){
            //1，增加用户余额,2余额记录表
            User::walletChange($data['pid'],'commission_income','money',$data['commission_price'],$data['id'],'佣金转入余额');
            $user = User::get($data['user_id']);
//            if(empty($user->is_bought)){
//                $user->is_bought = 1;
//                $user->save();
//            }
        }
        return $value;
    }





}
