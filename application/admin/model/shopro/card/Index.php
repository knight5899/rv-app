<?php

namespace app\admin\model\shopro\card;

use think\Model;
use traits\model\SoftDelete;

class Index extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'shopro_card';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'active_time_text',
    ];
    

    



    public function getActiveTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['active_time']) ? $data['active_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }



    protected function setActiveTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }
    protected function setExpireTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }
    protected function setUpdateTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }



    public function cardType()
    {
        return $this->belongsTo('app\admin\model\shopro\card\CardType', 'card_type_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    public function recommendUser()
    {
        return $this->belongsTo('app\admin\model\User', 'recommend_user_id', 'id', [], 'LEFT')->setEagerlyType(0)->bind('nickname');
    }

    public function user()
    {
        return $this->belongsTo('app\admin\model\User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }




}
