<?php

namespace app\admin\model\shopro\card;

use think\Model;
use traits\model\SoftDelete;

class CardType extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'shopro_card_type';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [

    ];

    public function getCateTypeList()
    {
        return ['1' => __('Fanche'), '2' => __('Way')];
    }
    

    







}
