<?php

namespace app\admin\model;

use think\Model;


class ShoproCard extends Model
{

    

    

    // 表名
    protected $name = 'shopro_card';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'active_time_text',
        'type_text',
        'upadtetime_text'
    ];
    

    
    public function getTypeList()
    {
        return ['day' => __('Type day'), 'saturday' => __('Type saturday')];
    }


    public function getActiveTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['active_time']) ? $data['active_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getUpadtetimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['upadtetime']) ? $data['upadtetime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setActiveTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setUpadtetimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


}
