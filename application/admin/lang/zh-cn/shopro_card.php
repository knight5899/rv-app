<?php

return [
    'Card_number'       => '卡号',
    'Title'             => '标题',
    'Desc'              => '内容',
    'Price'             => '价格',
    'Active_time'       => '激活时间',
    'Type'              => '类型',
    'Dayvip'            => '平日卡',
    'Alldayvip'         => '周末卡',
    'Active_code'       => '激活码',
    'Updatetime'        => '更新时间',
    'Createtime'        => '创建时间',
    'User_id'           => '购买用户',
    'User_idcard'       => '激活人身份证号',
    'Mobile'            => '激活人手机号',
    'Name'              => '激活人姓名',
    'Recommend_user_id' => '推荐官'
];
