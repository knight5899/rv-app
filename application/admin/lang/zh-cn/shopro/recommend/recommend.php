<?php

return [
    'Title'           => '名称',
    'Username'        => '姓名',
    'Mobile'          => '电话',
    'Message'         => '留言',
    'Createtime'      => '创建',
    'Updatetime'      => '更新',
    'Deletetime'      => '删除',
    'User_id'         => '用户',
    'Type'            => '类型',
    'Type 1'          => '申请推荐官',
    'Type 2'          => '房车托管合作',
    'Type 3'          => '申请合作商',
    'Status_switch'   => '审核',
    'Status_switch 1' => '通过',
    'Status_switch 0' => '拒绝'
];
