<?php

return [
    'Name'          => '名称',
    'Desciption'    => '内容',
    'Fanche'        =>'房车租赁',
    'Way'        =>'旅游路线',
    'Createtime'    => '创建时间',
    'Updatetime'    => '更新时间',
    'Deletetime'    => '删除时间',
    'Price'         => '现价',
    'Origin_price'  => '原价',
    'Cate_type'     => '栏目',
    'Limit_time'     => '节假日不可用',
    'Status_switch' => '开启'
];
