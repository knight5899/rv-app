<?php

return [
    'Card_number'       => '卡号',
    'Active_time'       => '激活时间',
    'Expire_time'       => '过期时间',
    'Card_type_id'      => '类型',
    'Use_status'       => '是否使用',
    'Active_code'       => '激活码',
    'Updatetime'        => '更新时间',
    'Createtime'        => '创建时间',
    'User_id'           => '购买用户',
    'User_idcard'       => '激活人身份证号',
    'Mobile'            => '激活人手机号',
    'Name'              => '激活人姓名',
    'Recommend_user_id' => '合作商',
    'Deletetime'        => '删除',
    'Open_switch'       => '开启'
];
