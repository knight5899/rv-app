<?php

return [
    'Order_id'         => '订单',
    'Goods_id'         => '商品',
    'Title'            => '商品标题',
    'Commission_price' => '一级佣金',
    'Commission_price2' => '二级佣金',
    'Status'           => '状态',
    'Status 0'         => '待审批',
    'Status 1'         => '审批完成',
    'Status 2'         => '撤消分红',
    'Createtime'       => '创建时间',
    'Updatetime'       => '更新时间',
    'Pid'              => '分销员',
    'User_id'          => '购买用户'
];
