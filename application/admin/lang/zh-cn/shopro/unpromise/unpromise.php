<?php

return [
    'User_id'      => '用户',
    'Order_id'     => '订单',
    'Channel_type' => '栏目类型',
    'Level'        => '类型',
    'Mark'         => '说明',
    'Createtime'   => '创建',
    'Open_time'    => '解除时间',
    'Updatetime'   => '更新',
    'Status'       => '处罚状态',
    'Status 1'     => '已处理',
    'Order_sn'     => '支付订单号',
    'Pay_time'     => '付款时间',
    'Pay_status'   => '支付状态',
    'Pay_price'    => '违约金'
];
