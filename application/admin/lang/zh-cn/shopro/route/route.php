<?php

return [
    'Place'      => '地点',
    'Start_time' => '出行时间',
    'Day_num'    => '出行天数',
    'People_num' => '出行人数',
    'Transport'  => '交通方式',
    'Username'   => '姓名',
    'Mobile'     => '手机',
    'Createtime' => '创建时间',
    'Updatetime' => '更新时间',
    'Deletetime' => '删除时间',
    'Mark'       => '备注',
    'User_id'    => '用户',
    'Type'       => '路线类型'
];
