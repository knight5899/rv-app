<?php

return [
    'Title'      => '标题',
    'Cover'      => '封面',
    'Video'      => '视频',
    'Content'    => '内容',
    'Createtime' => '时间',
    'Updatetime' => '更新'
];
