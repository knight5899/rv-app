define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shopro/schedule/schedule/index' + location.search,
                    add_url: 'shopro/schedule/schedule/add',
                    edit_url: 'shopro/schedule/schedule/edit',
                    del_url: 'shopro/schedule/schedule/del',
                    multi_url: 'shopro/schedule/schedule/multi',
                    import_url: 'shopro/schedule/schedule/import',
                    table: 'shopro_schedule',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'ld',
                sortName: 'ld',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'ld', title: __('Ld')},
                        {field: 'date', title: __('Date')},
                        {field: 'num', title: __('Num')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'order_id', title: __('Order_id')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});